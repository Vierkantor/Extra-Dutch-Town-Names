This NewGRF provides a extra-large set of almost-real Dutch town names.
The basic list is provided by http://www.metatopos.eu/almanak.html
(edition of 26th september 2018),
which has been manually analyzed into common pre- and suffixes,
to give a large collection of recognizable Dutch town names.
For example, you might find the real town name 'Waarder' in your game,
but also forms like 'Waarder aan de Maas' and/or 'Buiten-Waarder'.

The code for this NewGRF is based on Dutch Town Names for OpenTTD
by Jeroen Otto, which is licensed under the GNU General Public License
version 2 or later.
Apart from making their code free software, the authors of
Dutch Town Names have no connection to this NewGRF.
Please do not contact them for my mistakes!

This NewGRF does not require the setting of parameters.

CONTACT
-------

Please send any issues or pull requests to:
https://gitlab.com/Vierkantor/Extra-Dutch-Town-Names

Other contact details should also be available on that website.

LICENSE
-------
Extra Dutch Town Names
Copyright (C) 2018 by Vierkantor

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 (or, at your
discretion, any later version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the
Free Software Foundation, Inc.
1 Franklin Street, Fifth Floor
Boston, MA 02110-1301
USA.

---

Dutch Town Names for OpenTTD
Copyright (C) 2010-2012 by Jeroen Otto.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 (or, at your
discretion, any later version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the
Free Software Foundation, Inc.
1 Franklin Street, Fifth Floor
Boston, MA 02110-1301
USA.


CREDITS
-------

The original reference for the place names is Metatopos,
 accessed on 2018-10-01 at http://www.metatopos.eu/almanak.html.

The original source code for this project is Dutch Town Names by Jeroen Otto,
 which has the following credits:

 >  Authors:
 >      General coding:             Jeroen Otto (aka Hyronymus)
 >
 >  Translations:
 >      Dutch:                      Hyronymus
 >      English:                    Hyronymus
 >      German:                     planetmaker
 >
 >  Special thanks to #openttd and planetmaker for the continuous help in my
 >  fights with NML, Python and the devzone.

The list of base names, prefixes and suffixes, and especially the errors,
 were created by Vierkantor on the basis of the previously mentioned works.
